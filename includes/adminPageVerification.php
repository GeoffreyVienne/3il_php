<?php
	function isAdmin(bool $redirect=false){
		if(!isset($_SESSION['isAdmin']) || !$_SESSION['isAdmin']){
			if ($redirect) {
				header("Location: http://".$_SERVER['HTTP_HOST']);
				die();
			}
			return false;
		} else {
			return true;
		}
	}
?>