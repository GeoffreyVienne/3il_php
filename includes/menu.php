<?php
	$menu = array(
		'Accueil',
		'Galerie',
		'News',
		'Mon Compte'
	);
	if (isset($_SESSION['isAdmin'])&&$_SESSION['isAdmin']==1){
		$menu[4] = 'Espace Pro';
	}
?>
<nav>
	<ul>
		<?php foreach( $menu as $menuPage ) : ?>
		<li><a<?php if($menuPage == $_GET['currentPage']){echo ' class="active"';} ?> href="<?php echo "?currentPage=".$menuPage ; ?>"><?php echo $menuPage ; ?></a></li>
		<?php endforeach ?>
	</ul>
</nav>