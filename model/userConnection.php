<?php
	require 'model/DbUsers.php';
	function tryConnectUser($_login,$_password){
		$login = htmlspecialchars($_login);
		$password = htmlspecialchars($_password);
		
		//try to connect 
		$dbUsers = DbUsers::create(); 
		$user = $dbUsers->getUser($login);
		if(password_verify($password,$user->getPassword())){
			$_SESSION['user'] = $user->getLogin();
			$_SESSION['isAdmin'] = $user->isAdmin();
		} else {
			throw new Exception('Password does not correspond with user\'s password.');
		}
	}
	
	function disconnectUser(){
		session_unset();
		session_destroy();
	}
?>