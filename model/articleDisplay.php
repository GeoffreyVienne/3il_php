<?php
require 'model/DbArticles.php';
	function getArticles(){
		$dbArticles = DbArticles::create();
		$articles = $dbArticles->getArticles();
		return $articles;
	}
	function getArticle($id){
		$articles = getArticles();
		if (array_key_exists($id,$articles)){
			$article = $articles[$id];
			return $article;
		} else {
			throw new Exception("Given id does not correspond to any article in database.");
		}
	}
	function removeArticle($id){
		$dbArticles = DbArticles::create();
		$dbArticles->deleteArticle($id);
	}
	
	function modifyArticle($id,$title,$text){
		$dbArticles = DbArticles::create();
		$dbArticles->updateArticle($id,$title,$text);
	}
	
	function addArticle($title,$text,$author,$previewPicture){
		if(empty($title)){
			throw new Exception('Le titre ne doit pas être vide.');
		}
		if (empty($text)){
			throw new Exception('L\'article ne doit pas être vide.');
		}
		$dbArticles = DbArticles::create();
		$dbArticles->insertArticle($title,$text,$author,$previewPicture);
	}
?>