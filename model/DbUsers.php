<?php
	require 'model/db.connect.php';
	class User{
		private $login = "";
		private $password = "";
		private $admin = false;
		
		public function getLogin(){
			return $this->login;
		}
		
		public function getPassword(){
			return $this->password;
		}
		
		public function isAdmin(){
			return $this->admin;
		}
		
		public function __construct($_login,$_password,$_admin){
			$this->login = $_login;
			$this->password = $_password;
			$this->admin = $_admin;
		}
		
		public static function createFromRequest($request){
			$_login = $request->login;
			$_password = $request->passwd;
			$_admin = $request->isAdmin;
			$instance = new self($_login,$_password,$_admin);
			return $instance;
		}
	}
	
	class DbUsers{
		private $pdo = null;
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getUser($loginUser){
			$request = $this->pdo->prepare("SELECT * FROM user WHERE login = ?");
			if ($request->execute(array($loginUser)) != null){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					return User::createFromRequest($fetchedRequest[0]);
				} else {
					throw new Exception('User does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
	}
?>