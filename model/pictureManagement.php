<?php
function addPictureToFolder($picture,$target_dir){
	$target_file = $target_dir . basename($picture["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	// Check if image file is a actual image or fake image
	
	$check = false;
	if (!empty($picture["tmp_name"])){
		$check = getimagesize($picture["tmp_name"]);	
	}
	if($check !== false) {
		$uploadOk = 1;
	} else {
		throw new Exception('Le fichier n\'est pas une image.');
		$uploadOk = 0;
	}
	
	// Check if file already exists
	if (file_exists($target_file)) {
		throw new Exception('Le fichier existe déjà.');
		$uploadOk = 0;
	}
	// Check file size
	$maxSize = 5000000;
	if ($picture["size"] > $maxSize) {
		throw new Exception('Le fichier est trop lourd (>'.($maxSize/1000000).'Mo)');
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		throw new Exception('Seuls les formats JPG, JPEG, PNG et GIF sont authorisés.');
		$uploadOk = 0;
	}
	
	// if everything is ok, try to upload file
	if (!move_uploaded_file($picture["tmp_name"], $target_file)) {
		throw new Exception('Une erreur est survenue dans la création de fichier.');
	}
}

function addPicture($picture,$description){
	if (empty($description)){
		throw new Exception('La description de l\'image ne doit pas être vide.');
	}
	
	$target_dir = "resources/img/";
	addPictureToFolder($picture,$target_dir);
	
	//try to add to xml file
	$xmlFile = 'scripts/Carousel.xml';
	
	$dom = new DOMDocument();
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->load($xmlFile);
	$root=$dom->documentElement;
	
	$pictureNameNode = new DOMElement('IMG',$picture["name"]);
	$pictureDescriptionNode = new DOMElement('DESCRIPTION',$description);
	$newPictureNode = new DOMElement('PHOTO');
	
	$root->appendChild($newPictureNode);
	$newPictureNode->appendChild($pictureNameNode);
	$newPictureNode->appendChild($pictureDescriptionNode);
	
	$dom->save($xmlFile);
}

function removePicture($id){
	
	$target_dir = "resources/img/";
	$target_file = $target_dir.$id;
	
	if (!file_exists($target_file)){
		throw new Exception('Le fichier n\'existe pas.');
	}
	
	$xmlFile = 'scripts/Carousel.xml';
	
	$dom = new DOMDocument();
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->load($xmlFile);
	$root=$dom->documentElement;
	
	$pictures = $root->getElementsByTagName('PHOTO');
	$removedPictures = array();
	
	foreach($pictures as $picture){
		$pictureName = $picture->getElementsByTagName('IMG')->item(0);
		if($pictureName->textContent == $id ){
			array_push($removedPictures,$picture);
		}
	}
	foreach($removedPictures as $picture){
		$root->removeChild($picture);
	}
	
	$dom->save($xmlFile);
	
	unlink($target_file);
}
?>