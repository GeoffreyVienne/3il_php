<?php
	require 'model/db.connect.php';
	class Article{
		private $title = "";
		private $content = "";
		private $publicationDate = "";
		private $author = "";
		private $previewPicture = "";
		
		public function getTitle(){
			return $this->title;
		}
		
		public function getContent(){
			return $this->content;
		}
		
		public function getShortContent(){
			return substr($this->content,0,100)."...";
		}
		
		public function getPublicationDate(){
			return $this->publicationDate->format('d-m-Y H:i:s');
		}
		
		public function getPublicationDateOnly(){
			return $this->publicationDate->format('d-m-Y');
		}
		
		public function getAuthor(){
			return $this->author;
		}
		
		public function getPreviewPicture(){
			return $this->previewPicture;
		}
		
		public function __construct($_title,$_content,$_publicationDate,$_author,$_previewPicture){
			$this->title = $_title;
			$this->content = $_content;
			$this->publicationDate = $_publicationDate;
			$this->author = $_author;
			$this->previewPicture = $_previewPicture;
		}
		
		public static function createFromRequest($request){
			$_title = $request->title;
			$_content = $request->content;
			$_publicationDate = DateTime::createFromFormat('Y-m-d H:i:s',$request->publicationDate);
			$_author = $request->author;
			$_previewPicture = $request->previewId;
			$instance = new self($_title,$_content,$_publicationDate,$_author,$_previewPicture);
			return $instance;
		}
	}
	
	class DbArticles{
		private $pdo = null;
		
		public static function create(){
			$instance = new self(connect_to_db());
			return $instance;
		}
		
		public function __construct($_pdo){
			$this->pdo = $_pdo;
		}
		
		public function getArticles(){
			$request = $this->pdo->prepare("SELECT * FROM article ORDER BY publicationDate");
			if ($request->execute()){
				$fetchedRequest = $request->fetchAll();
				if (!empty($fetchedRequest)){
					$articles = array();
					foreach($fetchedRequest as $article){
						$articles[$article->id] = Article::createFromRequest($article);
					}
					return $articles;
				} else {
					throw new Exception('Article does not exist.');
				}
			} else {
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function insertArticle($title,$content,$author,$previewPicture){
			$request = $this->pdo->prepare("INSERT INTO article (title,content,author,previewId) VALUES (?,?,?,?)");
			if (!($request->execute(array($title,$content,$author,$previewPicture)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function deleteArticle($id){
			$request = $this->pdo->prepare("DELETE FROM article WHERE id=?");
			if (!($request->execute(array($id)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
		
		public function updateArticle($id,$title,$text,$previewPicture){
			$request = $this->pdo->prepare("UPDATE article SET content=?, title=?, previewId=? WHERE id=?");
			if (!($request->execute(array($text,$title,$id,$previewPicture)))){
				throw new Exception('Request failed. Contact an administrator to fix this issue.');
			}
		}
	}
?>