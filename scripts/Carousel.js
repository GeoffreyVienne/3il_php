//https://www.grafikart.fr/tutoriels/carrousel-javascript-87


class Carousel{
	
	
	
	constructor(element, options={}){
		this.element = element
		this.options = Object.assign({},{
			slidesToScroll: 1,
			slidesVisible:1
		}, options)
		
		this.isMobile = false
		if(window.innerWidth < 900){
			this.isMobile = true
		}
		
		
		let children = [].slice.call(element.children)
		this.currentSlide = 0
		this.root = this.createDivWithClass('carousel')
		this.container = this.createDivWithClass('carousel__container')
		
		this.root.appendChild(this.container)
		
		this.moveCallbacks = []
		this.element.appendChild(this.root)
		this.items = children.map((child) => {
			let item = this.createDivWithClass('carousel__item');
			
			if(document.getElementById('isAsdmin') !== null){
				console.log(document.getElementById('isAsdmin'))
				child.appendChild(this.createDelete(child.id));
			}
			
			item.appendChild(child);
			this.container.appendChild(item)
			return item
		})
		this.setStyle() 
		this.createNavigation()
		this.moveCallbacks.forEach(cb => cb(0))
		window.addEventListener('resize', this.onResize.bind(this))
	}
	
	setStyle(){
		let ratio = this.items.length / this.slidesVisible
		this.container.style.width = (ratio * 100) +"%"
		this.items.forEach((item)=>{
			item.style.width = ((100 / this.slidesVisible) / ratio)+"%"
		})
	}
	
	createNavigation(){
		let nextButton = this.createDivWithClass('carousel__next')
		let prevButton = this.createDivWithClass('carousel__prev')
		this.root.appendChild(nextButton)
		this.root.appendChild(prevButton)
		
		nextButton.addEventListener('click',this.next.bind(this))
		prevButton.addEventListener('click',this.prev.bind(this))
	}
	
	next(){
		this.goToItem(this.currentSlide + this.slidesToScroll)
	}
	
	prev(){
		this.goToItem(this.currentSlide - this.slidesToScroll)
	}
	
	goToItem(index){
		if(index < 0){
			index = this.items.length - this.slidesVisible
			
		}
		else if( index >= this.items.length ||
			(this.items[this.currentSlide + this.slidesVisible] === undefined 
			&& index > this.currentSlide)){
			index = 0
		}
		let transX = index * -100 / this.items.length
		this.container.style.transform = 'translate3d('+ transX +'%, 0, 0)'
		this.currentSlide = index
	}
	
	onMove(callBack){
		this.moveCallbacks.push(callBack)
	}
	
	onResize(){
		let mobile = window.innerWidth < 900
		
		if(mobile !== this.isMobile){
			this.isMobile = mobile
			this.setStyle()
			this.moveCallbacks.forEach(cb =>cb(this.currentSlide))
		}
	}
	
	createDivWithClass(className){
		let div = document.createElement('div')
		div.setAttribute('class', className)
		return div
	}
	
	createDelete(id){
		let div = this.createDivWithClass('deleteimg');
		let formulaire = document.createElement('form');
		formulaire.method = "post";
		
		let tooltip = this.createDivWithClass('tooltip');
		
		let input_button = document.createElement('input');
		input_button.setAttribute('class', 'stylebutton');
		input_button.classList.add("delete_img");
		input_button.type="submit";
		input_button.value="";
		input_button.setAttribute('onclick', "return confirm(\'Êtes vous sûr(e) de vouloir supprimer cette image?\') ")
		
		
		let span = document.createElement('span');
		span.setAttribute('class', 'tooltiptextLeft');
		span.textContent="Supprimer";
		
		tooltip.appendChild(input_button);
		tooltip.appendChild(span);
		
		let input_delete = document.createElement('input');
		input_delete.type="hidden";
		input_delete.name="delete";
		input_delete.value="1";
		
		let input_id = document.createElement('input');
		input_id.type="hidden";
		input_id.name="id";
		input_id.value=id;
		
		
		formulaire.appendChild(tooltip);
		formulaire.appendChild(input_delete);
		formulaire.appendChild(input_id);
		
		div.appendChild(formulaire);
		
		return div;
	}
	
	get slidesToScroll(){
		return this.isMobile? 1 : this.options.slidesToScroll
	}
	 
	get slidesVisible(){
		return this.isMobile? 1 : this.options.slidesVisible
	}
}

function createClass(){
	new Carousel(document.querySelector('#carousel1'),{
		slidesToScroll:1,
		slidesVisible:3
	})
}

