function getInfo(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var carousel= "";
  var x = xmlDoc.getElementsByTagName("PHOTO");
  for (i = 0; i <x.length; i++) { 
    carousel += "<div class=\"item\" id=\""+x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue+"\">" + 
	"<div class=\"item__image\">" +
	"<img alt=\"\" src=\"../resources/img/" + 
	x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue + 
	"\">"+
	"</div>" +
	"<div class=\"item__body\">" + 
	"<div class=\"item__description\">" +
	x[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue +
	"</div>" +
	"</div>" +
	"</div>";
  }
  carousel += "</div>"
  document.getElementById("carousel1").innerHTML = carousel;
}

function loadDoc() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		getInfo(this);
	  
		loadScript('scripts/AjaXML.js',
			createClass
		);
	}
  };
  xhttp.open("GET", "scripts/Carousel.XML", true);
  xhttp.send();
  
}
	
document.addEventListener('DOMContentLoaded', function(){
	loadDoc();
})

