<?php 
	include 'includes/session.php';
	require 'model/articleDisplay.php';
	require 'model/pictureManagement.php';
	include 'includes/adminPageVerification.php';
	isAdmin(true);
	
	if(isset($_POST['content'])){
		try {
			$directory = 'resources/img/articles/';
			$name = null;
			if (isset($_FILES['pictureToUpload'])){
				$name = $_FILES['pictureToUpload']['name'];
				addPictureToFolder($_FILES['pictureToUpload'],$directory);
			}
			addArticle($_POST['title'],$_POST['content'],$_SESSION['user'],$directory.$name);
			echo '<script language="Javascript"> alert("L\'article a été créé avec succès");';
			echo 'window.location.replace("?currentPage=News");</script>';
		} catch (Exception $e){
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<title>CielBlogue - Le Canard Déchainé</title>
<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<section>
	<div>
		<h2>Créer un Nouvel Article</h2>
	</div>
	
	<?php 
		if (isset($e)){
			echo '<div class="error">Error : '.$e->getMessage().'</div>';
		}
	?>
	
	<div>
		<form action="" method="post" enctype="multipart/form-data">
			Titre :<br/><input type="text" name="title" placeholder="Titre de l'article" /><br/>
			Contenu :<br/><textarea name="content" placeholder="Contenu de l'article"></textarea><br/>
			Fichier :<br/><input type="file" name="pictureToUpload"/><br/>
			<input type="submit" value="Créer l'article"/>
		</form>
	</div>
</section>

<?php include "includes/footer.php"?>

</body>
</html>