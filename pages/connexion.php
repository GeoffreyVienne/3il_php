<?php 
	include 'includes/session.php';
	require 'model/userConnection.php';
?>
<html>
<head>
	<title>CielBlogue - Le Canard Déchainé</title>
	<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/connexion.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php
	try{
		if(isset ($_POST['deconnexion'])){
			disconnectUser();
		}
		if(isset($_POST['login'],$_POST['password'])){
			tryConnectUser($_POST['login'],$_POST['password']);
		}
	} catch(Exception $e) {
		$errorMessage = $e->getMessage();
	}
	include "includes/menu.php";
	
	if(!isset($_SESSION['user'])){
		include "includes/connectionForm.php";
	} else {
		include "includes/accountInfo.php";
	}
?>
<?php include "includes/footer.php"?>
</body>
</html>