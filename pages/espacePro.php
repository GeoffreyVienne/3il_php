<?php 
	include 'includes/session.php';
	include 'includes/adminPageVerification.php';
	isAdmin(true);
?>
<!DOCTYPE html>
<html>
<head>
<title>CielBlogue - Le Canard Déchainé</title>
<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<section>
	<div>
		<h2>Admin</h2>
	</div>
	<div class="ProButton">
		<a class="stylebutton" href="?currentPage=Accueil&editionMode">Mise à jour page d'Accueil</a><br/>
		<a class="stylebutton" href="?currentPage=News">Gestion d'articles</a><br/>
		<a class="stylebutton" href="?currentPage=Galerie">Gestion d'images</a>
	</div>
</section>

<?php include "includes/footer.php"?>

</body>
</html>