<?php 
	include 'includes/session.php'; 
	include 'includes/adminPageVerification.php';
	include 'model/pictureManagement.php';
	
	if (isset($_POST['delete'])&&$_POST['delete']&&isAdmin()){
		try {
			removePicture($_POST['id']);
		} catch (Exception $e){
		}			
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>CielBlogue - Le Canard Déchainé</title>
	<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/Carousel.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/Tooltip.css">
	
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<?php
	if(isset($e)){
		echo '<div class="error">Error : '.$e->getMessage().'</div>';
	}
	
	if(isAdmin()){
		echo '<div id="isAsdmin" hide></div>';
	}
?>

<section>
	<div>
		<h2>Galerie</h2>
	</div>

	<div>
		 <div id="carousel1"> 
			
		</div>
	</div>
	
	<?php
	
		if(isAdmin()){
			
			echo '<a class="button stylebutton tooltip" href="?currentPage=Nouvelle Image"><span class="tooltiptext">Ajouter une image</span><span><img src="../resources/img/add.png"></img></span></a>';
		}
	?>
	
</section>

<script src="scripts/Carousel.js"></script>
<script src="scripts/Head.js"></script>
<script src="scripts/AjaXML.js"></script>

<?php include "includes/footer.php"?>
</body>
</html>