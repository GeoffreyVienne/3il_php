<?php 
	include 'includes/session.php';
	require 'model/articleDisplay.php';
	include 'includes/adminPageVerification.php';
	
	if (isset($_POST['delete'])&&$_POST['delete']&&isAdmin()){
		removeArticle($_POST['id']);
	}
	
	if(isset($_POST['content'])&&isAdmin()){
		modifyArticle($_POST['id'],$_POST['title'],$_POST['content']);
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>CielBlogue - Le Canard Déchainé</title>
	<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/article.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/tooltip.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<section>
	<div>
		<h2>News</h2>
	</div>
	
	<?php
		try {
			if (isset($_GET['id']))
			{
				$article = getArticle($_GET['id']);
				$title = $article->getTitle();
				$preview = $article->getPreviewPicture();
				$content = $article->getContent();
				$author = $article->getAuthor();
				$date = $article->getPublicationDate();
				
				if(isAdmin() && isset($_GET['editionMode'])){
					echo '<form action="" method="post">'.
						'Titre :<br/><input type="text" name="title" value="'.$title.'"/><br/>'.
						'Contenu :<br/><textarea name="content">'.htmlspecialchars($content).'</textarea><br/>'.
						'<input type="hidden" name="id" value="'.$_GET['id'].'"/>'.
						'<input type="submit" value="Mettre à jour"/>'.
					'</form>';
				} else {
					echo '<div class="article">'.
						'<img class="image" src="'.$preview.'"/>'.
						'<div class="title">'.$title.'</div>'.
						'<div class="content">'.nl2br($content).'</div>'.
						'<div class="author">'.$author.'</div>'.
						'<div class="date">'.$date.'</div>'.
					'</div>';
				}
				
				echo '<a href="?currentPage=News" class="button stylebutton tooltip return_button"><span class="tooltiptext">Retour</span><img src="../resources/img/Backward.jpg"></img></a>';
			} else {
				$articles = getArticles();
				
				if(isAdmin()){
					echo '<div>'.'<a href="?currentPage=Nouvel Article" class="button stylebutton">Nouvel Article</a>'.'</div>';
				}
				
				echo '<div class="row">';
				
				
				function getEditionTextFor($id){
					if (isAdmin()) {
						return '<div class="DeleteAdd">'.
							'<a class="button stylebutton tooltip" href="'.'?currentPage=News&id='.$id.'&editionMode'.'"><span class="tooltiptext">Modifier</span> <span><img src="../resources/img/edit.png"></img></span></a>'.
							'<form action="" method="post">'.
							'<div class="tooltip">'.
							'<input class="stylebutton DeleteArticle" type="submit" value="" onclick="return confirm(\'Êtes vous sûr(e) de vouloir supprimer cet article?\')"/>'.
							'<span class="tooltiptext">Supprimer</span> '.
							'</div>'.
							'<input type="hidden" name="delete" value="1"/>'.
							'<input type="hidden" name="id" value="'.$id.'"/>'.
							'</form>'.
							'</div>';
					}
				}
				
				foreach($articles as $id => $article){
					echo '<div class="article column">'.
						
							getEditionTextFor($id).
							'<a href="?currentPage=News&id='.$id.'">'.
							'<img class="preview" src="'.$article->getPreviewPicture().'"/>'.
							'<div class="title">'.$article->getTitle().'</div>'.
							'<div class="content">'.$article->getShortContent().'</div>'.
							'<div class="author">'.$article->getAuthor().'</div>'.
							'<div class="date">'.$article->getPublicationDateOnly().'</div>'.
						'</a>'.
					'</div>';
				}
				echo '</div>';
			}
		} catch (Exception $e) {
			echo '<div class="error">Error : '.$e->getMessage().'</div>';
		}
	?>
</section>

<?php include "includes/footer.php"?>

</body>
</html>