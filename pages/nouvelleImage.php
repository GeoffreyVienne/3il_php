<?php 
	include 'includes/session.php';
	require 'model/pictureManagement.php';
	include 'includes/adminPageVerification.php';
	isAdmin(true);
	
	if(isset($_POST['description'])){
		try {
			addPicture($_FILES["pictureToUpload"],$_POST['description']);
			echo '<script language="Javascript"> alert("L\'image a été ajoutée avec succès.");';
			echo 'window.location.replace("?currentPage=Galerie");</script>';
		} catch (Exception $e) {
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<title>CielBlogue - Le Canard Déchainé</title>
<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<section>
	<div>
		<h2>Ajouter une Nouvelle Image</h2>
	</div>
	<div>
		<?php 
			if (isset($e)){
				echo '<div class="error">Error : '.$e->getMessage().'</div>';
			}
		?>
	
		<form action="" method="post" enctype="multipart/form-data">
			Fichier :<br/><input type="file" name="pictureToUpload"/><br/>
			Description :<br/><input type="text" name="description" placeholder="Description de l'image" /><br/>
			<input type="submit" value="Ajouter l'image"/>
		</form>
	</div>
</section>

<?php include "includes/footer.php"?>

</body>
</html>