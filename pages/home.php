<!DOCTYPE html>
<?php
	include 'includes/session.php';
	include 'includes/adminPageVerification.php';
	if(isset($_GET['editionMode'])){
		isAdmin(true);
	}
	if(isset($_POST['homeText'])){
		$filename = 'resources/texts/home.txt';
		$file = fopen($filename,'w');
		fwrite($file,$_POST['homeText']);
		fclose($file);
		header("Location: http://".$_SERVER['HTTP_HOST']);
		die();
	}
?>
<html>
<head>
	<title>CielBlogue - Le Canard Déchainé</title>
	<link rel="stylesheet" type="text/css" href="../resources/css/sheet.css">
</head>
<body>

<header>
	<h1>CielbloguÀ - Le canard dechainÀ</h1>
</header>

<?php include "includes/menu.php"; ?>

<section>
	<div>
		<h2>Accueil</h2>
	</div>

	<div>
		<?php
			$filename = 'resources/texts/home.txt';
			if ( !file_exists("resources/texts") ){
				mkdir("resources/texts");
			}
			$file = file($filename);
			
			if (isset($_GET['editionMode'])){
				echo "Modification de la page d'accueil : ";
				echo '<form action="" method="post"><textarea name="homeText">';
				foreach($file as $line){
					echo $line;
				}
				echo '</textarea><br/><input type="submit" value="Mettre à jour" /></form>';
			} else {
				echo '<p>';
				foreach($file as $line){
					echo $line.'<br/>';
				}
				echo '</p>';
			}
		?>
	</div>
</section>

<?php include "includes/footer.php"?>

</body>
</html>