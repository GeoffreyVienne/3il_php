<?php
	$currentPage ="";
	if(!empty($_GET['currentPage'])){
		$currentPage = $_GET['currentPage'];
	}
	$includedPage = './pages/';
	switch ($currentPage){
		case "News":
			$includedPage .= 'news.php';
			break;
		case "Mon Compte":
			$includedPage .= 'connexion.php';
			break;
		case "Galerie":
			$includedPage .= 'galerie.php';
			break;
		case "Espace Pro":
			$includedPage .= "espacePro.php";
			break;
		case "Nouvel Article":
			$includedPage .= "nouvelArticle.php";
			break;
		case "Nouvelle Image":
			$includedPage .= "nouvelleImage.php";
			break;
		case "Accueil":
		default:
			$includedPage .= 'home.php';
			$_GET['currentPage'] = 'Accueil';
	}
	include($includedPage);
?>